//
//  LocationService.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import Foundation

class LocationService : LocationAPI {
	private let urlSession = URLSession.shared
	private let baseURL = URL(string: "https://restcountries.eu/rest/v2/all")!
	private let jsonDecoder: JSONDecoder = {
		let jsonDecoder = JSONDecoder()
		return jsonDecoder
	}()

	func fetchLocations(completion: @escaping (Result<[Location], APIServiceError>) -> Void) {
		urlSession.dataTask(with: baseURL) { data, res, err  in
			guard let statusCode = (res as? HTTPURLResponse)?.statusCode, 200..<299 ~= statusCode else {
				completion(.failure(.invalidResponse))
				return
			}
			do {
				let locations = try self.jsonDecoder.decode([Location].self, from: data!)
				completion(.success(locations))
			} catch let error {
				print(error)
				completion(.failure(.decodeError))
			}
			}.resume()
	}
}
