//
//  ClimaCellService.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import Foundation

//https://api.climacell.co/v3/weather/forecast/daily?lat=52&lon=20&start_time=now&end_time=2019-08-13T14%3A09%3A50Z&unit_system=si&fields=temp,precipitation_probability
class ClimaCellService: WeatherAPI {
	private let urlSession = URLSession.shared
	private let baseURL = URL(string: "https://api.climacell.co/v3/weather/forecast/daily")!
	private let apiKey = "mFW54hIC4r5puNkKBrcfQ3Xy3dqFYXCJ"
	private let jsonDecoder: JSONDecoder = {
		let jsonDecoder = JSONDecoder()
		return jsonDecoder
	}()
	
	func fetchWeather(for lat: Float, lon: Float, completion: @escaping (Result<[WeatherDayData], APIServiceError>) -> Void) {
		let dateIn4Days = Calendar.current.date(byAdding: .day, value: 4, to: Date())


		let endTime = dateIn4Days?.iso8601
		var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: true)
		let queryItems = [URLQueryItem(name: "lat", value: String(lat)),
						  URLQueryItem(name: "lon", value: String(lon)),
						  URLQueryItem(name: "start_time", value: "now"),
						  URLQueryItem(name: "end_time", value: endTime),
						  URLQueryItem(name: "unit_system", value: "si"),
						  URLQueryItem(name: "fields", value: "temp,precipitation_probability")]
		components?.queryItems = queryItems

		guard let url = components?.url else {
			return
		}

		var urlRequest = URLRequest(url: url)
		urlRequest.addValue("mFW54hIC4r5puNkKBrcfQ3Xy3dqFYXCJ", forHTTPHeaderField: "apikey")
		urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")

		urlSession.dataTask(with: urlRequest) { data, res, err  in
			guard let statusCode = (res as? HTTPURLResponse)?.statusCode, 200..<299 ~= statusCode else {
				completion(.failure(.invalidResponse))
				return
			}
			do {
				let weatherDays = try self.jsonDecoder.decode([WeatherDayData].self, from: data!)
				completion(.success(weatherDays))
			} catch let error {
				print(error)
				completion(.failure(.decodeError))
			}
			}.resume()
	}
}
