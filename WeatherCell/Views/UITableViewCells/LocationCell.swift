//
//  LocationCell.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {
	@IBOutlet weak var capital: UILabel!
	@IBOutlet weak var country: UILabel!
	@IBOutlet weak var flag: UIImageView!
}
