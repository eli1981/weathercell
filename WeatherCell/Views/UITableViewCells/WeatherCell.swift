//
//  WeatherCell.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import Foundation

class WeatherCell: UITableViewCell {
	@IBOutlet weak var day: UILabel!
	@IBOutlet weak var maxTemp: UILabel!
	@IBOutlet weak var minTemp: UILabel!
	@IBOutlet weak var pre: UILabel!
}
