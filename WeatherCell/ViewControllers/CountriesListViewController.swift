//
//  CountriesListViewController.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import UIKit

class CountriesListViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!

	let searchController = UISearchController(searchResultsController: nil)

	private let countriesListViewModel = CountriesListViewModel(apiService: LocationService())

	override func viewDidLoad() {
        super.viewDidLoad()

		//setup tableView
		tableView.delegate = self
		tableView.dataSource = self

		// Setup the Search Controller
		searchController.searchResultsUpdater = self
		searchController.obscuresBackgroundDuringPresentation = false
		searchController.searchBar.placeholder = "Search Locations"
		navigationItem.searchController = searchController
		definesPresentationContext = true

		countriesListViewModel.delegate = self
	}

	func searchBarIsEmpty() -> Bool {
		// Returns true if the text is empty or nil
		return searchController.searchBar.text?.isEmpty ?? true
	}

	func filterContentForSearchText(_ searchText: String, scope: String = "All") {
		countriesListViewModel.filteredLocations = countriesListViewModel.locations.filter({( location : Location) -> Bool in
			return location.capital.lowercased().contains(searchText.lowercased()) || location.country.lowercased().contains(searchText.lowercased())
		})

		tableView.reloadData()
	}

	func isFiltering() -> Bool {
		return searchController.isActive && !searchBarIsEmpty()
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let indexPath = self.tableView.indexPathForSelectedRow!

		var location: Location
		if isFiltering() {
			location = countriesListViewModel.filteredLocations[indexPath.row]
		} else {
			location = countriesListViewModel.locations[indexPath.row]
		}

		if segue.identifier == "DetailSegue" {

			if let destinationViewController = segue.destination as? CityDetailsViewController
			{
				destinationViewController.cityDetailsViewModel = CityWeatherViewModel(apiService: ClimaCellService(), selectedLocation: location)
			}
		}
	}

}

extension CountriesListViewController: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if isFiltering() {
			return countriesListViewModel.filteredLocations.count
		}

		return countriesListViewModel.locations.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath) as! LocationCell

		let location: Location
		if isFiltering() {
			location = countriesListViewModel.filteredLocations[indexPath.row]
		} else {
			location = countriesListViewModel.locations[indexPath.row]
		}

		cell.capital.text = location.capital
		cell.country.text = location.country

		var svgImage: SVGKImage = SVGKImage()
		DispatchQueue.background(background: {
			svgImage = SVGKImage(contentsOf: URL(string: location.flag))
		}, completion:{
			cell.flag.image = svgImage.uiImage
		})

		return cell
	}
}

extension CountriesListViewController: UITableViewDelegate {

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 80.0
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)

	}
}

extension CountriesListViewController: UISearchResultsUpdating {

	func updateSearchResults(for searchController: UISearchController) {
		filterContentForSearchText(searchController.searchBar.text!)
	}
}

extension CountriesListViewController: CountriesListDelegate  {
	func locationsArrayDidChange() {
		DispatchQueue.main.async {
			self.tableView.reloadData()
		}
	}

	func locatonsFetchhDidFail() {
		DispatchQueue.main.async {
			self.tableView.reloadData()
		}
	}
}

extension DispatchQueue {
	static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
		DispatchQueue.global(qos: .background).async {
			background?()
			if let completion = completion {
				DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
					completion()
				})
			}
		}
	}
}


