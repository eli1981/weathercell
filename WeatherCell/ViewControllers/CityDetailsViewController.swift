//
//  CityDetailsViewController.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import UIKit
import MapKit

class CityDetailsViewController: UIViewController {

	@IBOutlet weak var mapView: MKMapView!
	@IBOutlet weak var tableView: UITableView!

	var cityDetailsViewModel = CityWeatherViewModel(apiService: ClimaCellService(), selectedLocation: nil)

	lazy var dateFormatter: DateFormatter = {
		let dateFormatter = DateFormatter()
		return dateFormatter
	}()

	override func viewDidLoad() {
        super.viewDidLoad()

		cityDetailsViewModel.delegate = self

		tableView.dataSource = self
		tableView.delegate = self

		//clear separators of empty table view cells
		tableView.tableFooterView = UIView()
		tableView.separatorColor = UIColor.black
    }

	override func viewWillAppear(_ animated: Bool) {
		self.tableView.reloadData()
		self.loadMapLocation()
	}

	func loadMapLocation() {
		guard let location = cityDetailsViewModel.selectedLocation else {
			return
		}
		let newLocation = CLLocationCoordinate2D(latitude: CLLocationDegrees(location.latlng[0]), longitude: CLLocationDegrees(location.latlng[1]))

		//Center the map on the place location
		mapView.setCenter(newLocation, animated: false)
	}
}

extension CityDetailsViewController: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return cityDetailsViewModel.weatherData.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as! WeatherCell

		let dateString = cityDetailsViewModel.weatherData[indexPath.row].obsTime
		dateFormatter.dateFormat = "yyyy-MM-dd"
		let date = dateFormatter.date(from: dateString)
		dateFormatter.dateFormat = "MMM d"
		let newDateString = dateFormatter.string(from: date!)

		cell.day.text = newDateString
		cell.maxTemp.text = "\(String(Int(cityDetailsViewModel.weatherData[indexPath.row].maxTemp)))°"
		cell.minTemp.text = "\(String(Int(cityDetailsViewModel.weatherData[indexPath.row].minTemp)))°"
		cell.pre.text = String(cityDetailsViewModel.weatherData[indexPath.row].precipitation)

		return cell
	}
}

extension CityDetailsViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 107
	}
}

extension CityDetailsViewController: CityWeatherDelegate  {
	func weatherDataDidChange() {
		DispatchQueue.main.async {
			self.tableView.reloadData()
		}
	}

	func weatherFetchhDidFail() {
		DispatchQueue.main.async {
			self.tableView.reloadData()
		}
	}

	func locationDidChange() {
		self.loadMapLocation()
	}
}



