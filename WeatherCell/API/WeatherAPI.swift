//
//  WeatherAPI.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import Foundation

protocol WeatherAPI {
	func fetchWeather(for lat: Float, lon: Float, completion: @escaping (Result<[WeatherDayData], APIServiceError>) -> Void)
}
