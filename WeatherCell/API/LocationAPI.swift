//
//  LocationAPI.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import Foundation

protocol LocationAPI {
	func fetchLocations(completion: @escaping (Result<[Location], APIServiceError>) -> Void)
}

public enum APIServiceError: Error {
	case apiError
	case invalidResponse
	case noData
	case decodeError
}
