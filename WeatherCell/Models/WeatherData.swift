//
//  WeatherData.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import Foundation

//https://api.climacell.co/v3/weather/forecast/daily?lat=52&lon=20&start_time=now&end_time=2019-08-13T14%3A09%3A50Z&unit_system=si&fields=temp,precipitation_probability

struct WeatherDayData: Decodable {

	struct TempData: Decodable {

		struct Temp: Decodable {
			let value: Float

			// Coding Keys
			enum CodingKeys: String, CodingKey {
				case value
			}

			// Decoding
			init(from decoder: Decoder) throws {
				let container = try decoder.container(keyedBy: CodingKeys.self)
				value = try container.decode(Float.self, forKey: .value)
			}
		}

		let min: Temp?
		let max: Temp?

		// Coding Keys
		enum CodingKeys: String, CodingKey {
			case min
			case max
		}

		// Decoding
		init(from decoder: Decoder) throws {
			let container = try decoder.container(keyedBy: CodingKeys.self)
			min = try container.decodeIfPresent(Temp.self, forKey: .min)
			max = try container.decodeIfPresent(Temp.self, forKey: .max)
		}
	}

	struct PrecipitationData: Decodable {
		let value: Int

		//Coding Keys
		enum CodingKeys: String, CodingKey {
			case value
		}

		// Decoding
		init(from decoder: Decoder) throws {
			let container = try decoder.container(keyedBy: CodingKeys.self)
			value = try container.decode(Int.self, forKey: .value)
		}
	}

	struct ObservationTimeData: Decodable {
		let value: String

		//Coding Keys
		enum CodingKeys: String, CodingKey {
			case value
		}

		// Decoding
		init(from decoder: Decoder) throws {
			let container = try decoder.container(keyedBy: CodingKeys.self)
			value = try container.decode(String.self, forKey: .value)
		}
	}

	let minTemp: Float
	let maxTemp: Float
	let precipitation: Int
	let obsTime: String

	var temp: [TempData]
	let precProbability: PrecipitationData
	let observationTime: ObservationTimeData

	// Coding Keys
	enum CodingKeys: String, CodingKey {
		case temp
		case precProbability = "precipitation_probability"
		case observationTime = "observation_time"
	}

	// Decoding
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		precProbability = try container.decode(PrecipitationData.self, forKey: .precProbability)
		observationTime = try container.decode(ObservationTimeData.self, forKey: .observationTime)

		temp = []
		var tempContainer = try container.nestedUnkeyedContainer(forKey: .temp)
		while (!tempContainer.isAtEnd) {
			let value = try tempContainer.decode(TempData.self)
			temp.append(value)
		}

		minTemp = temp[0].min!.value
		maxTemp = temp[1].max!.value

		precipitation = precProbability.value
		obsTime = observationTime.value
	}

}



