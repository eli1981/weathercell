//
//  Location.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

struct Location: Decodable {
	let country: String
	let capital: String
	let flag: String
	var latlng: [Float]

	// Coding Keys
	enum CodingKeys: String, CodingKey {
		case country = "name"
		case capital
		case flag
		case latlng
	}

	// Decoding
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		country = try container.decode(String.self, forKey: .country)
		capital = try container.decode(String.self, forKey: .capital)
		flag = try container.decode(String.self, forKey: .flag)

		latlng = []
		var latlngContainer = try container.nestedUnkeyedContainer(forKey: .latlng)
		while (!latlngContainer.isAtEnd) {
			let value = try latlngContainer.decode(Float.self)
			latlng.append(value)
		}
	}
}
