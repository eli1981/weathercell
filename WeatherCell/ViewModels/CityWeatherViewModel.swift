//
//  CityWeatherViewModel.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import Foundation

protocol CityWeatherDelegate {
	func weatherDataDidChange()
	func weatherFetchhDidFail()
}

class CityWeatherViewModel {

	var weatherData: [WeatherDayData] = [] {
		didSet {
			delegate?.weatherDataDidChange()
		}
	}

	var delegate: CityWeatherDelegate?
	var service: WeatherAPI
	var selectedLocation: Location?

	init(apiService: WeatherAPI, selectedLocation: Location?) {
		self.service = apiService
		self.selectedLocation = selectedLocation
		fetchWeather()
	}

	func fetchWeather() {
		guard let location = self.selectedLocation else {
			return
		}
		service.fetchWeather(for: location.latlng[0], lon: location.latlng[1]) { result in
			switch result {
			case .success(let weatherData):
				self.weatherData = weatherData
				self.delegate?.weatherDataDidChange()
			case .failure(let error):
				print(error.localizedDescription)
				self.weatherData = [WeatherDayData]()
				self.delegate?.weatherFetchhDidFail()
			}
		}
	}
}



