//
//  CountriesListViewModel.swift
//  WeatherCell
//
//  Created by Lior on 09/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import Foundation


protocol CountriesListDelegate {
	func locationsArrayDidChange()
	func locatonsFetchhDidFail()
}

class CountriesListViewModel {

	var locations: [Location] = [] {
		didSet {
			delegate?.locationsArrayDidChange()
		}
	}

	var filteredLocations = [Location]()
	var delegate: CountriesListDelegate?
	var service: LocationAPI

	init(apiService: LocationAPI) {
		self.service = apiService
		fetchLocations()
	}

	func fetchLocations() {
		service.fetchLocations { result in
			switch result {
			case .success(let locations):
				self.locations = locations
			case .failure(let error):
				print(error.localizedDescription)
				self.locations = [Location]()
				self.delegate?.locatonsFetchhDidFail()
			}
		}
	}
}
